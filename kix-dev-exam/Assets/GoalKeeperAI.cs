﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalKeeperAI : MonoBehaviour {

	[SerializeField]
	Animator animator;
	MainController mainController;
	void Start()
	{
		mainController = MainController.instance;
		mainController.shootDelegate += PlayRandomCatchAnimation;
	}
	void PlayRandomCatchAnimation()
	{
		int randomAnimNumber = Random.Range(0, 7);
		switch (randomAnimNumber)
		{
			case 0:
				animator.SetTrigger("SideUpLeft");
				break;
			case 1:
				animator.SetTrigger("SideMidLeft");
				break;
			case 2:
				animator.SetTrigger("SideDownLeft");
				break;
			case 3:
				animator.SetTrigger("SideUpRight");
				break;
			case 4:
				animator.SetTrigger("SideMidRight");
				break;
			case 5:
				animator.SetTrigger("SideDownRight");
				break;
			case 6:
				animator.SetTrigger("Front");
				break;
			default:
				animator.SetTrigger("Front");
				break;
		}
	}
}
