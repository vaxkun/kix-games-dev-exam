﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour {

	#region Singleton
	public static MainController instance;
	void Awake()
	{
		instance = this;
	}

	#endregion

	[Header("Line")]
	[SerializeField]
	LineRenderer shootIndicator;
	[Header("Ball")]
	[SerializeField]
	BallController ballController;
	[SerializeField]
	float shootForce;
	[SerializeField]
	Vector3 shootDirection;
	[Header("Striker")]
	[SerializeField]
	StrikerAnimatorController strikerAnimController;

	public delegate void ShootDelegate();
	public ShootDelegate shootDelegate;

	void Start()
	{
		shootDelegate += StrikerShoot;
	}
	public void SetLinePositions(Vector3 start, Vector3 end)
	{
		shootIndicator.enabled = true;
		shootIndicator.SetPosition(0, start);
		shootIndicator.SetPosition(1, end);
		Invoke("HideLine", 0.5f);
	}
	void HideLine()
	{
		shootIndicator.enabled = false;
	}
	public void ShootBall()
	{
		ballController.ShootBall(shootDirection, shootForce);
	}
	public void SetShootForce(float newShootForce)
	{
		shootForce = newShootForce;
	}
	public void SetDirectionVector(Vector3 newDirection)
	{
		shootDirection = newDirection;
	}
	public void StrikerShoot()
	{
		strikerAnimController.PlayShootAnimation();
	}

}
