﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour {


	[Header("Vectors")]
	[SerializeField]
	Vector3 startPos;
	[SerializeField]
	Vector3 endPos;

	[Header("Components")]
	[SerializeField]
	Transform ballTransform;
	[SerializeField]
	Camera mainCam;
	MainController mainController;

	void Start()
	{
		mainController = MainController.instance;
	}
	void Update()
	{
		if (Input.GetMouseButtonDown(0))//If started tapping
		{
			CalculateStartSwipePosition();
		}
		else if (Input.GetMouseButtonUp(0))//If finished tapping
		{
			CalculateEndSwipePosition();
			mainController.SetLinePositions(ballTransform.position, endPos);
			mainController.SetShootForce(Vector3.Distance(startPos, endPos));
			mainController.SetDirectionVector((endPos - ballTransform.position));
			mainController.shootDelegate();
		}

	}

	void CalculateStartSwipePosition()
	{
		startPos = Input.mousePosition;
		startPos.z = 36f;
		startPos = Camera.main.ScreenToWorldPoint(startPos);
	}
	void CalculateEndSwipePosition()
	{
		Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		if(Physics.Raycast(ray,out hit))
		{
			endPos = hit.point;
		}
	}

}
