﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StrikerAnimatorController : MonoBehaviour {

	[SerializeField]
	Animator animator;

	public void PlayShootAnimation()
	{
		animator.SetTrigger("Shoot");
	}
	public void ShootEvent()
	{
		MainController.instance.ShootBall();
	}
}
