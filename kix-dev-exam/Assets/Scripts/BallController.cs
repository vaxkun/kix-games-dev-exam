﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour {

	[SerializeField]
	Rigidbody rigidbody;

	public void ShootBall(Vector3 direction,float force)
	{
		rigidbody.AddForce(direction * force*0.2f, ForceMode.Impulse);
	}
}
